MessageScreen = class('MessageScreen')

function MessageScreen:initialize()
  self.message_time = 5
  self.fade_time = 1
  self.padding = 20

  -- Probably should abstract states.
  self.current_state = "fade_in"
  self.state_time = 0

  self.messages = {"There is no shame in giving up if you think you've had too many! ... There is, however, shame in jet vomiting your friend's bathroom",
                   "Maybe you should try drinking sugar drinks instead of alcohol and enjoy the sugar rush",
                   "Maybe you need to take a few minutes to consider when your life took the turn in which you started playing a videogame just about drinking",
                   "Winners don't drink! ... Losers, however, do... a lot. So try not to lose!",
                   "If you begin to notice your dependency to alcohol is becoming a burden in your life, you should consider professional help. Also, you kinda suck at this game",
                   "Al-anon is always open to new members. Contact your nearest chapter!",
                   "You don't look half as cool as you think you do. You're getting drunk, what can you possibly know?",
                   "Do not, under any circumstances, attempt to play this game with liquor. What are you, crazy?",
                   "The fact that this game requires 2 players or more is for your own safety. If you find yourself playing by yourself anyways, please contact professional help",
                   "If you play this game frequently, you may expect an intervention soon. You're hurting us all :("}
  self.current_message = self.messages[ math.random( #self.messages )]

  -- Set the screen black
  love.graphics.setBackgroundColor(0, 0, 0);
end

function MessageScreen:update(dt)
  self.state_time = self.state_time + dt

  -- update the time
  if self.current_state == "fade_in" and self.state_time >= self.fade_time then
    self.current_state = "showing_text"
    self.state_time = 0
  end

  if self.current_state == "fade_out" and self.state_time >= self.fade_time then
    screen:swap_screen("GameScreen")
  end

  if self.current_state == "showing_text" and self.state_time >= self.message_time then
    self.current_state = "fade_out"
    self.state_time = 0
  end
end

function MessageScreen:draw(dt)
  self:draw_text(self.current_message, 200)
end

function MessageScreen:unload()
  -- Garbage Collected
end

function MessageScreen:draw_text(message, y, opacity)
  love.graphics.setColorMode("modulate")
  love.graphics.setColor(255,255,255,self:text_opacity())
  love.graphics.printf(message, self.padding, y, love.graphics.getWidth()-2*self.padding, "center")
  love.graphics.setColor(0,0,0,255)
  love.graphics.setColorMode("replace")
end

function MessageScreen:text_opacity()
  if self.current_state == "fade_in" then
    return math.floor(self.state_time * 255 / self.fade_time)
  elseif self.current_state == "fade_out" then
    return math.floor(255 - (self.state_time * 255 / self.fade_time))
  else
    return 255
  end
end
