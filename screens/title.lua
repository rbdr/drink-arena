TitleScreen = class('TitleScreen')

function TitleScreen:initialize()
  self.title_image = love.graphics.newImage('sprites/static/title.gif')
  self.background_image = nil

  love.graphics.setBackgroundColor(173, 216, 199);

  -- track the key so we don't get repeats.
  self.keylock = false
end

function TitleScreen:update(dt)
  -- Update the bg block position. Also possibly deal with the splash

  if love.keyboard.isDown( "left" ) and not self.keylock then
    self.keylock = true
    self:decrease_players()
  end

  if love.keyboard.isDown( "right" ) and not self.keylock then
    self.keylock = true
    self:increase_players()
  end

  if love.keyboard.isDown( "z" ) and not self.keylock then
    self.keylock = true
    screen:swap_screen("MessageScreen")
  end

  if not love.keyboard.isDown("left", "right", "z") then
    self.keylock = false
  end
end

function TitleScreen:draw(dt)
  love.graphics.draw(self.title_image, (love.graphics:getWidth()/2-self.title_image:getWidth()), 50, 0, 2, 2)
  self:draw_text("Players: < "..game.players.." >", 350)
  self:draw_text("Press 'z' to start!!", 400)
  self:draw_text("(C) 2012 Ben Beltran", 650)
end

function TitleScreen:unload()
  -- Garbage Collected
end

function TitleScreen:draw_text(message, y)
  love.graphics.setColorMode("modulate")
  love.graphics.setColor(0,0,0,255)
  love.graphics.printf(message, 0, y, love.graphics.getWidth(), "center")
  love.graphics.setColorMode("replace")
end

function TitleScreen:increase_players()
  game.players = game.players + 1
  if game.players > game.max_players then
    game.players = game.min_players
  end
end

function TitleScreen:decrease_players()
  game.players = game.players - 1
  if game.players < game.min_players then
    game.players = game.max_players
  end
end
