GameScreen = class('GameScreen')

function GameScreen:initialize()
  self.actor_manager = ActorManager:new()
  love.graphics.setBackgroundColor(173, 216, 199);
  self.keylock = false
  player = Player:new(self.actor_manager, 64, 64, 'sprites/static/heart.png')
  bullet_factory_1 = RingActorFactory:new(self.actor_manager, 200,200, nil, nil, 25, -1, 2)
  bullet_factory_2 = RingActorFactory:new(self.actor_manager, 200,200, nil, nil, 25, -1, 2, .25, 2*math.pi/50)
  bullet_factory_ring = DirectionalRingActorFactory:new(self.actor_manager, 200, 200, nil, nil, nil, 10, -1, 1.5)
end

function GameScreen:update(dt)
  self.actor_manager:update(dt)
  bullet_factory_1:update(dt) -- Do something about this once you've figured 'em out
  bullet_factory_2:update(dt) -- Do something about this once you've figured 'em out
  bullet_factory_ring:update(dt) -- Do something about this once you've figured 'em out

  -- Handle the esc key
  if love.keyboard.isDown( "escape" ) and not self.keylock then
    self.keylock = true
    screen:swap_screen("TitleScreen")
  end

  if not love.keyboard.isDown("escape") then
    self.keylock = false
  end
end

function GameScreen:draw(dt)
  self.actor_manager:draw(dt)
  self:draw_text("Press ESC to return to title.", 650)
end

function GameScreen:unload()
  -- Garbage Collected
end

function GameScreen:draw_text(message, y)
  love.graphics.setColorMode("modulate")
  love.graphics.setColor(0,0,0,255)
  love.graphics.printf(message, 0, y, love.graphics.getWidth(), "center")
  love.graphics.setColorMode("replace")
end
