require('lib/vendor/middleclass')
table.inspect = require('lib/vendor/inspect')
require('lib/extra_math')

require('game')

require('actor_manager')
require('actors/actor')
require('actors/player')
require('actors/bullet_line')

require ('actors/factories/ring')
require ('actors/factories/directional_ring')

require ('screen_manager')
require ('screens/title')
require ('screens/message')
require ('screens/game')

entities = {}

function love.load()

  --configuration (this should probably be its own file, um, TODO)
  next_time = love.timer.getMicroTime()

  --set scale mode
  love.graphics.setDefaultImageFilter( 'nearest', 'nearest' )

  --Don't add actors this way, use overwatch:move_in()

  -- Load Game, Screen Manager and Actor Manager
  game = Game:new()
  screen = ScreenManager:new("TitleScreen")
end

function love.update(dt)
  --limit frames
  next_time = next_time + (1/game.fps)

  --Run the update code for every actor
  --An overwatch class could handle this.
  screen:update(dt)
end

function love.draw(dt)

  --Same as update, draw per actor.
  screen:draw(dt)

  --DEBUG: show fps
  debug_message(love.timer.getFPS(), 10, 10)

  --limit the time
  local cur_time = love.timer.getMicroTime()
  if next_time <= cur_time then
    next_time = cur_time
    return
  end
  love.timer.sleep(next_time - cur_time)
end

function debug_message(message, x, y)
  love.graphics.setColorMode("modulate")
  love.graphics.setColor(0,0,0,255)
  love.graphics.print(message, x, y)
  love.graphics.setColorMode("replace")
end

function love.quit()
  print("Goodbye!")
end
