ActorManager = class('ActorManager')

function ActorManager:initialize()
  self.actors = {}
  self.main_actor = nil
end

function ActorManager:update(dt)
  for pos, actor in pairs(self.actors) do
    actor:update(dt)
  end
end

function ActorManager:draw(dt)
  for pos, actor in pairs(self.actors) do
    actor:draw(dt)
  end
end

function ActorManager:move_in(actor)
  table.insert(self.actors, actor)
end

function ActorManager:move_out(actor)
  for pos, current_actor in pairs(self.actors) do
    if current_actor == actor then
      table.remove(self.actors, pos)
    end
  end
end

function ActorManager:set_main(actor)
  self.main_actor = actor
end
