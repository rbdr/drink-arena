Game = class('Game')

function Game:initialize()
  -- TODO: Maybe move this to a config manager
  self.max_players = 4
  self.min_players = 2
  self.fps = 30

  self.players = self.min_players
  self.current_player = self.min_players

  -- Set the font for the whole game. srsly
  local font = love.graphics.newImageFont("fonts/manaspace-white.gif",
      " ABCDEFGHIJKLMNOPQRSTUVWXYZ" ..
      "abcdefghijklmnopqrstuvwxyz" ..
      "0123456789" ..
      "`~!@#$%^&*()-_=+[]{}\\|;:'\",.<>/?")
  love.graphics.setFont(font)
end
