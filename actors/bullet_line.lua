BulletLine = class('BulletLine', Actor)

function BulletLine:initialize(actor_manager, x, y, image, direction)
  Actor.initialize(self, actor_manager, x, y, image)

  self.vel = 2

  -- Assume direction is in radians.
  self.direction = direction or 0
end

function BulletLine:draw(dt)
  love.graphics.draw(self.image, self.x, self.y, math.pi * self.direction, self.scale, self.scale, self.image:getWidth() / 2, self.image:getHeight() / 2)

  --draw bounding box.
  self:draw_bounding_box()
end

function BulletLine:update(dt)
  self.x = self.x + (self.vel * math.cos(self.direction))
  self.y = self.y + (self.vel * math.sin(self.direction))

  if self.x + self.image:getWidth()*self.scale < 0 or self.x > love.graphics.getWidth() then
    if self.y + self.image:getHeight()*self.scale < 0 or self.y > love.graphics.getHeight() then
      self:destroy()
    end
  end
end
