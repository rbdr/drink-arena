Actor = class('Actor')

function Actor:initialize(actor_manager, x, y, image)

  self.actor_manager = actor_manager

  --set properties with the constructor, with defaults
  self.x = x or 0
  self.y = y or 0
  self.image = love.graphics.newImage(image or "sprites/static/bullet-white.png")
  self.name = "Generic Actor"

  self.scale = 2

  self.collide = true
  self.ai = nil
  self.friendly = true
  self.fps = 12
  self.immortal = false
  self.life = 10

  --bounding box
  local box_width = self.image:getWidth() * self.scale
  local box_height = self.image:getHeight() * self.scale
  self.box = {}
  self.box.top = math.floor(box_height/2)
  self.box.bottom = math.floor(box_height/2)
  self.box.left = math.floor(box_width/2)
  self.box.right = math.floor(box_width/2)

  --movement variables
  self.max_vel = 5
  self.x_vel = 0          -- Current Velocities
  self.y_vel = 0

  self.actor_manager:move_in(self)
end

function Actor:destroy()
  self.actor_manager:move_out(self)
end

function Actor:draw()
  love.graphics.draw(self.image, self.x - self.image:getWidth() * self.scale / 2, self.y - self.image:getHeight() * self.scale / 2, 0, self.scale, self.scale)
end

function Actor:draw_bounding_box()
  love.graphics.setColor(255, 0, 0, 128)
  love.graphics.rectangle("line", self.x-self.box["left"], self.y-self.box["top"], self:box_width(), self:box_height())
  love.graphics.setColor(255,255,255,255)
end

function Actor:update(dt)
  --Most of the actor code goes here.
  self:physics(dt)
end

function Actor:physics(dt)
  --move in x
  self.x = math.floor(self.x + self.x_vel)
  self:unstick("x")

  --move in y
  self.y = math.floor(self.y + self.y_vel)
  self:unstick("y")
end

function Actor:unstick(context)
  if context == "y" then
    while not self:can_pass("down") do
      self.y = self.y-1
    end

    while not self:can_pass("up") do
      self.y = self.y+1
    end
  end

  if context == "x" then
    while not self:can_pass("right") do
      self.x = self.x-1
    end

    while not self:can_pass("left") do
      self.x = self.x+1
    end
  end
end

function Actor:can_pass(direction, x, y)
  -- Check if can pass
  return true
end

function Actor:box_width()
  return self.box.right+self.box.left
end

function Actor:box_height()
  return self.box.top+self.box.bottom
end
