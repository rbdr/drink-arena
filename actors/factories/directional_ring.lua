DirectionalRingActorFactory = class('DirectionalRingActorFactory')

function DirectionalRingActorFactory:initialize(actor_manager, x, y, radius, direction, actor_type, bullets, repetitions, delay, time_offset, angle_offset)
  self.actor_manager = actor_manager
  print(self.actor_manager.main_actor)
  self.x = x
  self.y = y
  self.radius = radius or 25
  self.bullets = bullets or 10
  self.repetitions = repetitions or 0 -- -1 repetitions for infinite factory
  self.delay = delay or 3.5 -- delay is in seconds
  self.actor_type = actor_type or "BulletLine"
  self.angle_offset = angle_offset or 0
  self.direction = direction

  time_offset = time_offset or 0

  -- get the interval
  self.interval = 2*math.pi / self.bullets

  self.last_generation = self.delay - time_offset -- instant first gen
  self.times_generated = 0
end

function DirectionalRingActorFactory:update(dt)
  if self.last_generation >= self.delay and self:may_generate() then
    self:generate()
    self.last_generation = self.last_generation - self.delay
    self.times_generated = self.repetitions + 1
  else
    self.last_generation = self.last_generation+dt
  end
end

function DirectionalRingActorFactory:generate(dt)
  local angle = self.angle_offset
  for i = 1, self.bullets do
    local x = self.x+self.radius*math.cos(angle)
    local y = self.y+self.radius*math.sin(angle)
    local actor_class = loadstring("return "..self.actor_type)()

    local current_direction = nil
    if self.direction == nil then
      current_direction = self:get_direction()
    else
      current_direction = self.direction
    end

    local actor = actor_class:new(self.actor_manager, x, y, nil, current_direction)
    actor.vel = 10

    angle = angle + self.interval
  end
end

function DirectionalRingActorFactory:may_generate()
  if self.repetitions >= 0 then
    if self.times_generated > self.repetitions then
      return false
    end
  end

  -- NOTE: maybe add an "active" state

  return true
end

function DirectionalRingActorFactory:get_direction()
  local dx = self.actor_manager.main_actor.x - self.x
  local dy = self.actor_manager.main_actor.y - self.y
  return math.atan2(dy,dx)
end
