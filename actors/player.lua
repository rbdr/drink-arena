Player = class('Player', Actor)

function Player:initialize(actor_manager, x, y, image)
  Actor.initialize(self, actor_manager, x, y, image)

  self.name = "Player"

  self.actor_manager:set_main(self)

  --define the individual sprite width and height
  self.sprite_width = 64
  self.sprite_height = 64

  self.max_vel = 7      -- Maximum Velocity

  self.key_times = {}        -- Key input times

  local particleimage = love.graphics.newImage('sprites/static/heart.png')

  self.particle = love.graphics.newParticleSystem(particleimage, 20)
  self.particle:setEmissionRate(10)
  self.particle:setSpeed(20, 50)
  self.particle:setGravity(0, -250)
  self.particle:setSizes(1.5, .5)
  self.particle:setColors(204, 0, 0, 255, 204, 0, 0, 0)
  self.particle:setPosition(400, 300)
  self.particle:setLifetime(1)
  self.particle:setParticleLife(1)
  self.particle:setDirection(-190)
  self.particle:setSpread(100)
  self.particle:setRadialAcceleration(100, 30)
  self.particle:stop()
end

function Player:draw()
  love.graphics.draw(self.particle, 0, 0)
  love.graphics.setColorMode('replace')
  love.graphics.draw(self.image, self.x - self.image:getWidth() * self.scale / 2, self.y - self.image:getHeight() * self.scale / 2, 0, self.scale, self.scale)

  --draw bounding box.
  self:draw_bounding_box()
end

function Player:update(dt)
  --regular keyboard / joypad control
  --TODO: Move these "right, left, etc." to a config file. Much laters though.
  --NOTE: Joystick buttons mapped to xbox controller

  if love.keyboard.isDown( "right" ) then
    self:update_time("right")
  else
    self.key_times["right"] = 0
  end

  if love.keyboard.isDown( "left" ) then
    self:update_time("left")
  else
    self.key_times["left"] = 0
  end


  if love.keyboard.isDown( "up" ) then
    self:update_time("up")
  else
    self.key_times["up"] = 0
  end

  if love.keyboard.isDown( "down" ) then
    self:update_time("down")
  else
    self.key_times["down"] = 0
  end

  if self.key_times["right"] > 0 and self.key_times["right"] > self.key_times["left"] then
    self.x_vel = self.max_vel
  end

  if self.key_times["left"] > 0 and self.key_times["left"] > self.key_times["right"] then
    self.x_vel = -self.max_vel
  end

  if self.key_times["up"] > 0 and self.key_times["up"] > self.key_times["down"] then
    self.y_vel = -self.max_vel
  end

  if self.key_times["down"] > 0 and self.key_times["down"] > self.key_times["up"] then
    self.y_vel = self.max_vel
  end

  -- reset on keyup
  if self.key_times["left"] == 0 and self.key_times["right"] == 0 then
    self.x_vel = 0
  end

  if self.key_times["up"] == 0 and self.key_times["down"] == 0 then
    self.y_vel = 0
  end

  self.particle:setPosition(self.x, self.y)
  self.particle:start()
  self.particle:update(dt)

  self:physics(dt)
end

function Player:update_time(direction)
  if self.key_times[direction] == 0 then
    self.key_times[direction] = love.timer.getMicroTime()
  end
end

function Player:can_pass(direction, x, y)

  x = x or self.x
  y = y or self.y

  if (direction == "up" and self.y < 0) then
    return false
  end

  if (direction == "down" and self.y > love.graphics:getHeight()) then
    return false
  end

  if (direction == "left" and self.x < 0) then
    return false
  end

  if (direction == "right" and self.x > love.graphics:getWidth()) then
    return false
  end

  return true
end
