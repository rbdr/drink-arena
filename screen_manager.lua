ScreenManager = class('ScreenManager')

function ScreenManager:initialize(screen)
  self:load_screen(screen)
end

function ScreenManager:swap_screen(screen_name)
  self:unload_screen(self.current_screen)
  self:load_screen(screen_name)
end

function ScreenManager:load_screen(screen_name)
  self.current_screen = loadstring("return "..screen_name..":new()")()
end

function ScreenManager:unload_screen(screen)
  screen:unload()
  self.current_screen = nil
end

function ScreenManager:update(dt)
  self.current_screen:update(dt)
end

function ScreenManager:draw(dt)
  self.current_screen:draw(dt)
end
